﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    Rigidbody2D rigidbody;
    public float Speed;
    public float JumpHight;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        float movement = Input.GetAxis("Horizontal");

        transform.Translate(Vector2.right * movement * Time.deltaTime * Speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rigidbody.AddRelativeForce(Vector2.up * JumpHight, ForceMode2D.Impulse);
        }
    }
}
