﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public Player mainPlayer;
    public float lastEnemyPos;
    public float currentEnemyPos;
    public float Speed = 5;
    public float JumpHight = 700;
    public bool isBlocked;
    public bool isMoving;

    public float jumpNext = 0.0f;
    public float jumpRate = 0.5f;


	void Start () {
        mainPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        CheckPlayerPosition();
        isMoving = true;
        //StartCoroutine("CheckIfBlocked");
	}
	
	// Update is called once per frame
	void FixedUpdate () {


        if (MoveLeft() && isMoving)
        {
            transform.Translate(Vector2.right * -1 * Time.deltaTime * Speed);
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
        }
        else
        {
            transform.Translate(Vector2.right * Time.deltaTime * Speed);
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
        }



        if (isBlocked && Time.time > jumpNext)
        {
            isMoving = false;
            gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * JumpHight, ForceMode2D.Impulse);
            Debug.Log("Jump!");
            isBlocked = false;
            isMoving = true;
            jumpNext = Time.time + jumpRate;

        }
    }

    public Vector2 CheckPlayerPosition() {
        Vector2 playerPos = new Vector2(mainPlayer.transform.position.x, mainPlayer.transform.position.y);
        return playerPos;
    }

    public bool MoveLeft() {
        float calculatedMov;
        bool result;
        calculatedMov = CheckPlayerPosition().x - gameObject.transform.position.x;
        if (calculatedMov < 0)
        {
            result = true;
        }
        else {
            result = false;
        }
        return result;

    }



    //IEnumerator CheckIfBlocked() {

    //    lastEnemyPos = gameObject.transform.position.x;
    //    yield return new WaitForSeconds(1.0f);
    //    currentEnemyPos = gameObject.transform.position.x;
    //    Debug.Log("Check if blocked: " + lastEnemyPos + " " + currentEnemyPos);
    //    if (currentEnemyPos > lastEnemyPos - 0.2f && currentEnemyPos < lastEnemyPos + 0.2f)
    //    {
    //        isBlocked = true;
    //        Debug.Log("Should jump");
    //    }
    //    else {
    //        isBlocked = false;
    //    }

    //}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Jump") {
            isBlocked = true;
        }

    }




}
